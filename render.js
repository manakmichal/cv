var CV = {
  data: null,
  create: function(data){
    this.data = data;

    this.render();
  },
  render: function(){
    var header = this.content.createHeader();
    header.insertBefore("body script");

    if(this.data.urls === undefined){
      var links = this.content.createLinks();
      links.insertBefore("body script");
    }
  },
  content: {
    createHeader: function(){
      var data = CV.data;

      var el = $("<header></header>"),
          h1 = $("<h1></h1>"),
          h2 = $("<h2></h2>"),
          p = $("<p></p>");

      console.log(this.parent);

      if(data.basic.name){
        h1.text(data.basic.name);
        h1.appendTo(el);
      } else {
        throw "Missing name within Basic section";
      }

      if(data.basic.title){
        h2.text(data.basic.title);
        h2.appendTo(el);
      } else {
        throw "Missing title within Basic section";
      }

      if(data.basic.description){
        p.text(data.basic.description);
        p.appendTo(el);
      }

      return el;
    },
    createLinks: function(){
      var data = CV.data;

      var el = $("<section></section>");

      $.each(data.basic.urls, function(i, val){
        var a = $("<a></a>");

        a.attr("href", val);
        a.text(i);

        a.appendTo(el);
      });

      el.addClass("container-links");

      return el;
    }
  }
}
